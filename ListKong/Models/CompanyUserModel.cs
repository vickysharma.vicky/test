﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class CompanyUserModel
    {
        public string Id { get; set; }
        public string RoleId { get; set; }

        [Required(ErrorMessage = "Company  is required!")]       
        [Display(Name = "Company")]
        public Int64 CompanyId { get; set; }

        [Required(ErrorMessage = "Role  is required!")]
        [Display(Name = "Role")]        
        public string RoleName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Mininum length for first name is 2!")]
        [MaxLength(100, ErrorMessage = "Maximum length for first name is 100!")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Mininum length for last name is 2!")]
        [MaxLength(100, ErrorMessage = "Maximum length for last name is 100!")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email  is required!")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password  is required!")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    public class CompanyUserListModel
    {
        public string Id { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }
        [Display(Name = "Role")]
        public string RoleName { get; set; }
        public string RoleId { get; set; }
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        public Int64 CompanyId { get; set; }
    }
}
