﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class DropdownModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
