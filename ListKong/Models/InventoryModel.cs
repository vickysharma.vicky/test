﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class InventoryListModel
    {

        public int InvID { get; set; }
        public int InvLocationID { get; set; }
        [DisplayName("Location Name")]
        public string LocationName { get; set; }
        public int InvCompanyID { get; set; }

        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Inventory name is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for inventory name is 100!")]
        [DisplayName("Inventory Name")]
        public string InvName { get; set; }
        [Required(ErrorMessage = "Inventory desc is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory desc is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for inventory desc is 500!")]
        [DisplayName("Inventory Desc")]
        public string InvDesc { get; set; }

        [DisplayName("Created On")]
        public DateTime InvCreatedDate { get; set; }

        public int? InvStatus { get; set; }

        [DisplayName("Status")]
        public string StatusName { get; set; }

    }
    public class InventoryModel
    {

        public int InvID { get; set; }
        public int InvLocationID { get; set; }
        [DisplayName("Location Name")]
        [Required(ErrorMessage = "Location Name is required!")]
        public string LocationName { get; set; }
        public int InvCompanyID { get; set; }

        [DisplayName("Company Name")]
        [Required(ErrorMessage = "Company Name is required!")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Inventory name is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for inventory name is 100!")]
        [DisplayName("Inventory Name")]
        public string InvName { get; set; }
        [Required(ErrorMessage = "Inventory desc is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory desc is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for inventory desc is 500!")]
        [DisplayName("Inventory Desc")]
        public string InvDesc { get; set; }

        [DisplayName("Created On")]
        public DateTime InvCreatedDate { get; set; }
        [NotMapped]
        public List<InventoryImageModel> InventoryImages { get; set; }
    }
    public class InventoryImageModel
    {
        public IFormFile InventoryImage { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Int64 ImageId { get; set; }

    }
    public class GalleryImageModel
    {
        public Int64 InvId { get; set; }
        public Int64 InvCompanyID { get; set; }
        public IFormFile InventoryImage { get; set; }
    }
    public class GalleryResult
    {
        public string imagePath { get; set; }
        public Int64 imageId { get; set; }
        public string status { get; set; }
    }
}
