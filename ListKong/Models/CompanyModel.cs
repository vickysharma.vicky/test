﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class CompanyModel
    {
        public long CompanyId { get; set; }
        [Required]
        [MinLength(3, ErrorMessage = "Mininum length for company name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for company name is 100!")]
        [Display(Name ="Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Mininum length for contact person is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for contact person is 100!")]
        [Display(Name = "Contact Person")]
        public string ContactName { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Mininum length for address is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for address is 100!")]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Mininum length for location name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for location name is 100!")]
        [Display(Name = "Location Name")]
        public string LocationName { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Mininum length for location description is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for location description is 500!")]
        [Display(Name = "Location Description")]
        public string LocationDescription { get; set; }
    }
    public class CompanyListModel
    {
        [Display(Name = "Company Id")]
        public int CompanyId { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Contact Person")]
        public string ContactName { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }
    }
}
