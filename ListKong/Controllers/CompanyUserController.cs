﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    public class CompanyUserController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;
        private readonly UserManager<ApplicationUser> _userManager;

        public CompanyUserController(IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
            this._userManager = userManager;
        }
        #endregion
        public IActionResult Index()
        {
            DataTable _dtResponseP = CompanyUserListByUserId();
            if (this._objHelper.checkDBNullResponse(_dtResponseP))
            {
                List<CompanyUserListModel> lst = new List<CompanyUserListModel>();
                lst = ExtensionMethods.DataTableToList<CompanyUserListModel>(_dtResponseP);
                return View(lst);
            }
            
            return View();
        }
        public IActionResult CreateUser()
        {
            var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            CompanyUserModel model = new CompanyUserModel();            
            ViewData["Roles"] = RoleList();
            ViewData["CompanyByAdminUser"] = CompanyList();
            return View(model);
        }
        [HttpPost]
        public IActionResult CreateUser(CompanyUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email,FirstName=model.FirstName,LastName=model.LastName };
            var result = _userManager.CreateAsync(user, model.Password).Result;
            if (result.Succeeded)
            {
                var role = _userManager.AddToRoleAsync(user, model.RoleName).Result;
                var currentUser = _userManager.FindByNameAsync(user.UserName).Result;
                var output = AddUserInCompany(currentUser.Id, model.CompanyId);
                TempData["SuccessMessage"] = "Company user successfully added!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["ErrorMessage"] = result.Errors.FirstOrDefault().Description;
                return View(model);
            }
        }
        public IActionResult EditCompanyUser(string Id)
        {
            CompanyUserModel model = new CompanyUserModel();
            ViewData["Roles"] = RoleList();
            ViewData["CompanyByAdminUser"] = CompanyList();
            DataTable _dtResponseP = CompanyUserDetailById(Id);
            if (this._objHelper.checkDBNullResponse(_dtResponseP))
            {
                List<CompanyUserModel> lst = new List<CompanyUserModel>();
                lst = ExtensionMethods.DataTableToList<CompanyUserModel>(_dtResponseP);
                return View(lst.FirstOrDefault());
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult EditCompanyUser(CompanyUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ViewData["Roles"] = RoleList();
            ViewData["CompanyByAdminUser"] = CompanyList();

            var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@Id", model.Id));
            parameters.Add(new KeyValuePair<string, string>("@FirstName", model.FirstName));
            parameters.Add(new KeyValuePair<string, string>("@LastName", model.LastName));
            parameters.Add(new KeyValuePair<string, string>("@Email", model.Email));
            parameters.Add(new KeyValuePair<string, string>("@RoleName", model.RoleName));
            parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(model.CompanyId)));
            parameters.Add(new KeyValuePair<string, string>("@UpdatedBy", Convert.ToString(loggedInUserId)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Update_User_Detail_Created_By_Admin", parameters);

            return View(model);
        }
        private int AddUserInCompany(string userId,Int64 CompanyId)
        {
            var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@UserId", userId));
            parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(CompanyId)));
            parameters.Add(new KeyValuePair<string, string>("@CreatedBy", Convert.ToString(loggedInUserId)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_New_Company_User", parameters);
            return 1;
        }

        private DataTable CompanyUserListByUserId()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_All_User_Created_By_Admin_User", parameters);
        }
        private DataTable CompanyUserDetailById(string userId)
        {
            //var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_User_Detail_Created_By_Admin_By_Id", parameters);
        }
        private DataTable RoleList()
        {
            return _objDataHelper.ExecuteProcedure("app_Get_All_Role_For_Dropdown", null);
        }
        private DataTable CompanyList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_Companies_Created_By_Admin_User_Dropdow", parameters);
        }
    }
}