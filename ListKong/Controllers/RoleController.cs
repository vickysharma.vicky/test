﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ListKong.Controllers
{
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> roleManager;
        UserManager<ApplicationUser> _userManager;
        public RoleController(RoleManager<IdentityRole> roleMgr, UserManager<ApplicationUser> _user)
        {
            roleManager = roleMgr;
            _userManager = _user;
        }
        public IActionResult Index()
        {
            //IdentityResult result = roleManager.CreateAsync(new IdentityRole("Super Admin")).Result;
            //IdentityResult result1 = roleManager.CreateAsync(new IdentityRole("Admin")).Result;
            //IdentityResult result2 = roleManager.CreateAsync(new IdentityRole("Photographer")).Result;
            //IdentityResult result3 = roleManager.CreateAsync(new IdentityRole("Inventory Manager")).Result;
            //IdentityResult result4 = roleManager.CreateAsync(new IdentityRole("Lister")).Result;

            //var user = new ApplicationUser { UserName = "vickysharma.vicky@gmail.com", Email = "vickysharma.vicky@gmail.com"};
            //var role =  _userManager.AddToRoleAsync(user, "Admin").Result;

            return View(roleManager.Roles);
        }

        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create([Required]string name)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                    Errors(result);
            }
            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await roleManager.DeleteAsync(role);
                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                    Errors(result);
            }
            else
                ModelState.AddModelError("", "No role found");
            return View("Index", roleManager.Roles);
        }

        private void Errors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
                ModelState.AddModelError("", error.Description);
        }
    }
}