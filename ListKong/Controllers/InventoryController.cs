﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    [Authorize]
    public class InventoryController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;

        public InventoryController(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
        }
        #endregion
        public IActionResult Index()
        {
            try
            {
                DataTable _dtResponseP = InventoryList();
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    List<InventoryListModel> lst = new List<InventoryListModel>();
                    lst = ExtensionMethods.DataTableToList<InventoryListModel>(_dtResponseP);
                    return View(lst);
                }
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View();
            }
            return View();
        }
        public IActionResult AddInventory()
        {
            InventoryModel model = new InventoryModel();
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@role", role));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Get_Company_Location_By_User_Id", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                model.InvCompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                model.InvLocationID = Convert.ToInt32(dt.Rows[0]["LocID"]);
                model.CompanyName = Convert.ToString(dt.Rows[0]["cCompanyName"]);
                model.LocationName = Convert.ToString(dt.Rows[0]["LocName"]);
            }
            if (role == "Admin")
            {
                ViewData["CompanyByAdminUser"] = CompanyList();
            }


            return View(model);
        }

        [HttpPost]
        public IActionResult AddInventory(InventoryModel model)
        {
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            if (!ModelState.IsValid)
            {
                if (role == "Admin")
                {
                    ViewData["CompanyByAdminUser"] = CompanyList();
                }
                return View(model);
            }
            try
            {
                var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@userId", userId));
                parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(model.InvCompanyID)));
                parameters.Add(new KeyValuePair<string, string>("@locationId", Convert.ToString(model.InvLocationID)));
                parameters.Add(new KeyValuePair<string, string>("@inventoryName", model.InvName));
                parameters.Add(new KeyValuePair<string, string>("@inventoryDesc", model.InvDesc));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_New_Inventory", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ViewData["SuccessMessage"] = "Inventory successfully added!";
                    model.InvID = Convert.ToInt32(dt.Rows[0]["InventoryID"]);
                }
                DataTable dtTemp = new DataTable();
                dtTemp.Columns.Add("ImageName", typeof(string));
                dtTemp.Columns.Add("ImagePath", typeof(string));
                foreach (var item in model.InventoryImages)
                {
                    string fileName = model.InvID + "_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + '_' + item.InventoryImage.FileName;
                    string FilePath = FileUploadUtility.UploadFile(item.InventoryImage, Convert.ToString(model.InvCompanyID), fileName);
                    dtTemp.Rows.Add(fileName, FilePath);
                }
                using (SqlConnection db = new SQLGateway(cnnect).CreateConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("app_Add_Inventory_Images"))
                    {
                        cmd.Connection = db;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@InventoryId", model.InvID);
                        cmd.Parameters.AddWithValue("@CreatedBy", userId);
                        cmd.Parameters.AddWithValue("@InventoryImageTableType", dtTemp);
                        db.Open();
                        cmd.ExecuteNonQuery();
                        db.Close();
                        cmd.Dispose();
                    }
                }
                //RedirectToAction("AddInventory");
                TempData["SuccessMessage"] = "Inventory successfully added!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            return View();
        }

        public IActionResult Edit(string Id)
        {
            InventoryModel model = new InventoryModel();
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@InvID", Id));
            DataSet ds = _objDataHelper.ExecuteProcedureWithDataSet("app_Get_Inventory_By_Id", parameters);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.InvID = Convert.ToInt32(Id);
                    model.CompanyName = Convert.ToString(ds.Tables[0].Rows[0]["CompanyName"]);
                    model.LocationName = Convert.ToString(ds.Tables[0].Rows[0]["LocationName"]);
                    model.InvCompanyID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvCompanyID"]);
                    model.InvLocationID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvLocationID"]);
                    model.InvName = Convert.ToString(ds.Tables[0].Rows[0]["InvName"]);
                    model.InvDesc = Convert.ToString(ds.Tables[0].Rows[0]["InvDesc"]);
                    model.InvCreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["InvCreatedDate"]);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    model.InventoryImages = new List<InventoryImageModel>();
                    foreach (DataRow item in ds.Tables[1].Rows)
                    {
                        InventoryImageModel imageModel = new InventoryImageModel(); 
                        imageModel.FileName ="/"+ Path.Combine(Convert.ToString(item["ImagePath"]), Convert.ToString(item["ImageName"]));
                        imageModel.FilePath = Convert.ToString(item["ImagePath"]);
                        imageModel.ImageId = Convert.ToInt64(item["ImageId"]);
                        model.InventoryImages.Add(imageModel);
                    }
                }
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Edit(InventoryModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(model.InvCompanyID)));
            parameters.Add(new KeyValuePair<string, string>("@locationId", Convert.ToString(model.InvLocationID)));
            parameters.Add(new KeyValuePair<string, string>("@inventoryName", model.InvName));
            parameters.Add(new KeyValuePair<string, string>("@inventoryDesc", model.InvDesc));
            parameters.Add(new KeyValuePair<string, string>("@InvID", Convert.ToString(model.InvID)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Update_Inventory", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                ViewData["SuccessMessage"] = "Inventory successfully updated!";
                model.InvID = Convert.ToInt32(dt.Rows[0]["InventoryID"]);
            }
            TempData["SuccessMessage"] = "Inventory successfully updated!";
            return RedirectToAction("Index");
        }

        public JsonResult GetLocationByCompany(string id)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(id)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Get_Location_By_Company_Id", parameters);
            string locationName = "";
            string locationId = "";
            string result = "error";
            if (dt != null && dt.Rows.Count > 0)
            {
                result = "success";
                locationName = Convert.ToString(dt.Rows[0]["LocName"]);
                locationId = Convert.ToString(dt.Rows[0]["LocID"]);
            }
            return Json(new { result = result,locName= locationName , locId= locationId });
        }
        public JsonResult ApproveInventory(string id)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@invId", Convert.ToString(id)));
            parameters.Add(new KeyValuePair<string, string>("@userId", Convert.ToString(userId)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Approve_Inventory_By_Id", parameters);
            string result = "error";
            if (dt != null && dt.Rows.Count > 0)
            {
                result = "success";
            }
            return Json(new { result = result });
        }
        public string DeleteImage(Int64 imageId)
        {
            string result = "Error";
            try
            {
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@imageId", Convert.ToString(imageId)));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Delete_Inventory_Image_By_Id", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    result = "Ok";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message.ToString();
            }
            return result;
        }
        public IActionResult Gallery(string Id)
        {
            InventoryModel model = new InventoryModel();
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@InvID", Id));
            DataSet ds = _objDataHelper.ExecuteProcedureWithDataSet("app_Get_Inventory_By_Id", parameters);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.InvID = Convert.ToInt32(Id);
                    model.CompanyName = Convert.ToString(ds.Tables[0].Rows[0]["CompanyName"]);
                    model.LocationName = Convert.ToString(ds.Tables[0].Rows[0]["LocationName"]);
                    model.InvCompanyID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvCompanyID"]);
                    model.InvLocationID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvLocationID"]);
                    model.InvName = Convert.ToString(ds.Tables[0].Rows[0]["InvName"]);
                    model.InvDesc = Convert.ToString(ds.Tables[0].Rows[0]["InvDesc"]);
                    model.InvCreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["InvCreatedDate"]);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    model.InventoryImages = new List<InventoryImageModel>();
                    foreach (DataRow item in ds.Tables[1].Rows)
                    {
                        InventoryImageModel imageModel = new InventoryImageModel();
                        imageModel.FileName = "/" + Path.Combine(Convert.ToString(item["ImagePath"]), Convert.ToString(item["ImageName"]));
                        imageModel.FilePath = Convert.ToString(item["ImagePath"]);
                        imageModel.ImageId = Convert.ToInt64(item["ImageId"]);
                        model.InventoryImages.Add(imageModel);
                    }
                }
                else
                {
                    model.InventoryImages = new List<InventoryImageModel>();
                }

            }
            return View(model);
        }
        [HttpPost]
        public GalleryResult AddGalleryImage(GalleryImageModel model)
        {

            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            GalleryResult result = new GalleryResult();
            result.status = "error";
            string fileName = model.InvId + "_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + '_' + model.InventoryImage.FileName;
            string FilePath = FileUploadUtility.UploadFile(model.InventoryImage, Convert.ToString(model.InvCompanyID), fileName);
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@InvId", Convert.ToString(model.InvId)));
            parameters.Add(new KeyValuePair<string, string>("@FilePath", Convert.ToString(FilePath)));
            parameters.Add(new KeyValuePair<string, string>("@FileName", fileName));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_Inventory_Gallery_Image", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                result.status= Convert.ToString(dt.Rows[0]["success"]);
                result.imageId = Convert.ToInt32(dt.Rows[0]["imageId"]);
            }
            string returnResult = "/" + FilePath + "/" + fileName;
            result.imagePath = returnResult;
            return result;
        }
        public DataTable InventoryList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@role", role));
            return _objDataHelper.ExecuteProcedure("app_Get_All_Inventory", parameters);
        }
        private DataTable CompanyList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_Companies_Created_By_Admin_User_Dropdow", parameters);
        }
    }
}