﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;

        public CompanyController(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
        }
        #endregion
        public async Task<IActionResult> Index()
        {
            try
            {
                DataTable _dtResponseP = CompanyList();
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    List<CompanyListModel> lst = new List<CompanyListModel>();
                    lst = ExtensionMethods.DataTableToList<CompanyListModel>(_dtResponseP);
                    return View(lst);
                }
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View();
            }
            return View();
        }
        public async Task<IActionResult> AddCompany()
        {
            CompanyModel model = new CompanyModel();            
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AddCompany(CompanyModel model)
        {

            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@userId", userId));
                parameters.Add(new KeyValuePair<string, string>("@Address", Convert.ToString(model.Address)));
                parameters.Add(new KeyValuePair<string, string>("@CompanyName", Convert.ToString(model.CompanyName)));
                parameters.Add(new KeyValuePair<string, string>("@ContactName", model.ContactName));
                parameters.Add(new KeyValuePair<string, string>("@Email", model.Email));
                parameters.Add(new KeyValuePair<string, string>("@LocationName", model.LocationName));
                parameters.Add(new KeyValuePair<string, string>("@LocationDescription", model.LocationDescription));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_New_Company", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ViewData["SuccessMessage"] = "Company successfully added!";
                }
              
                //RedirectToAction("AddInventory");
                TempData["SuccessMessage"] = "Company successfully added!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            return View();
        }
       
        public async Task<IActionResult> Edit(long? id)
        {
            CompanyModel model = new CompanyModel();
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(id)));
            DataSet ds = _objDataHelper.ExecuteProcedureWithDataSet("app_Get_Company_By_Id", parameters);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.CompanyId = Convert.ToInt32(id);
                    model.CompanyName = Convert.ToString(ds.Tables[0].Rows[0]["CompanyName"]);
                    model.LocationName = Convert.ToString(ds.Tables[0].Rows[0]["LocationName"]);
                    model.Address = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                    model.ContactName = Convert.ToString(ds.Tables[0].Rows[0]["ContactName"]);
                    model.Email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                    model.LocationDescription = Convert.ToString(ds.Tables[0].Rows[0]["LocationDescription"]);
                }
            }
            return  View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(CompanyModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(model.CompanyId)));
            parameters.Add(new KeyValuePair<string, string>("@CompanyName", Convert.ToString(model.CompanyName)));
            parameters.Add(new KeyValuePair<string, string>("@LocationName", model.LocationName));
            parameters.Add(new KeyValuePair<string, string>("@Address", model.Address));
            parameters.Add(new KeyValuePair<string, string>("@ContactName", Convert.ToString(model.ContactName)));
            parameters.Add(new KeyValuePair<string, string>("@Email", model.Email));
            parameters.Add(new KeyValuePair<string, string>("@LocationDescription", model.LocationDescription));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Update_Company", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                ViewData["SuccessMessage"] = "Company successfully updated!";
            }
            TempData["SuccessMessage"] = "Company successfully updated!";
            return RedirectToAction("Index");
        }
        public DataTable CompanyList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_All_Company_By_Admin_User", parameters);
        }
    }
}