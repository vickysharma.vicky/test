﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Utility
{
    public static class FileUploadUtility
    {
        public static string UploadFile(IFormFile file, string CompanyId ,string newFileName = null)
        {
            string filePath = string.Empty;

            if (file == null || file.Length == 0)
            {
                filePath = "Error";
                return filePath;
            }
            string folderPath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/httpdocs/Inventory/"+ CompanyId);
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

                var path = Path.Combine(
                        folderPath,
                        newFileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            filePath = "httpdocs/Inventory/" + CompanyId;

            return filePath;
        }
    }
}
