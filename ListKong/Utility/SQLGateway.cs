﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Utility
{
    public class SQLGateway
    {
        private string _connectionString;

        public SqlConnection CreateConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public SQLGateway(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public DataTable ExecuteProcedure(string procedure_name, List<KeyValuePair<string, string>> param_list)
        {
            SqlConnection _conn = CreateConnection();
            _conn.Open();
            SqlCommand cmd = new SqlCommand("", _conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procedure_name;

            if (param_list != null)
            {
                for (int i = 0; i < param_list.Count; i++)
                {
                    cmd.Parameters.AddWithValue(param_list[i].Key, param_list[i].Value);
                }
            }

            SqlDataAdapter _da = new SqlDataAdapter(cmd);
            DataSet _ds = new DataSet();

            _conn.Close();

            _da.Fill(_ds, "ResponeData");

            return _ds.Tables["ResponeData"];
        }

        public DataSet ExecuteProcedureWithDataSet(string procedure_name, List<KeyValuePair<string, string>> param_list)
        {
            SqlConnection _conn = CreateConnection();
            _conn.Open();
            SqlCommand cmd = new SqlCommand("", _conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procedure_name;
            if (param_list != null)
            {
                for (int i = 0; i < param_list.Count; i++)
                {
                    cmd.Parameters.AddWithValue(param_list[i].Key, param_list[i].Value);
                }
            }

            SqlDataAdapter _da = new SqlDataAdapter(cmd);
            DataSet _ds = new DataSet();

            _da.Fill(_ds, "ResponeData");

            _conn.Close();

            return _ds;
        }

        public DataTable ExecuteQuery(string query)
        {
            SqlConnection _conn = CreateConnection();
            _conn.Open();

            SqlCommand cmd = new SqlCommand(query, _conn);

            SqlDataAdapter _da = new SqlDataAdapter(cmd);
            DataSet _ds = new DataSet();

            _da.Fill(_ds, "ResponeData");

            _conn.Close();

            return _ds.Tables["ResponeData"];
        }



    }
}
